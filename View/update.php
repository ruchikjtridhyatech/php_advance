<? session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Update User</title>
	<link rel="stylesheet" href="../assets/css/style.css">

</head>
<body>
	<div class="form">
		<form accept-charset="utf-8" action="update_user.php" method="POST" enctype="multipart/form-data">
		<table class="tbl">
			<h2>Update Form</h2>
			<tbody>
				<tr>
					<td>Fisrt Name:</td>
					<td><input type="text" name="fname" id="fname" value="<?php echo $user['first-name'] ?>" ></td>
					<td><p id="err_fname">*Please Enter Your First Name!</p></td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td><input type="text" name="lname" id="lname" value="<?php echo $user['last-name'] ?>" ></td>
					<td><p id="err_lname">*Please Enter Your Last Name!</p></td>
				</tr>
				<tr>
					<td>Gender :</td>
					<td><label>Male<input type="radio" name="gender" id="gender" value="male" <?php echo ($user['gender']=='male' ? 'checked' : ''); ?> required></label>
						<label>Female<input type="radio" name="gender" id="gender" value="female" <?php echo 
						($user['gender']=='female' ? 'checked' : ''); ?> ></label></td>
					<td><p id="err_gender">*Please select your gender!</p></td>
				</tr>
				<?php $hobby=explode(",", $user['hobby']); ?>
				<tr>
					<td>Hobby :</td>
					<td>
						<label>Reading<input type="checkbox" <?php echo (in_array("Reading", $hobby) ? 'checked' : ''); ?> name="hobby[]" id="hobby"  class="dancing" value="Reading" ></label>
						<label>Surfing<input type="checkbox" <?php echo (in_array("Surfing", $hobby) ? 'checked' : ''); ?> name="hobby[]" id="hobby" class="singing" value="Surfing" ></label>
						<label>Cricket<input type="checkbox" <?php echo (in_array("Cricket", $hobby) ? 'checked' : ''); ?> name="hobby[]" id="hobby" class="sports" value="Cricket" ></label>
					</td>
					<td><p id="err_hoobie">*Please select any Hoobie!</p></td>
				</tr>
				<tr>
					<td>Phone Number: </td>
					<td><input type="number" value="<?php echo $user['phone'] ?>" name="num" id="num" ></td>
					<td><p id="err_num">*Number is not valid</p></td>
				</tr>
				<tr>
					<td>Photo: </td>
					<td><img class="dsb_img" style="float:left; margin-bottom: 10px" src="../assets/images/<?php echo $user['image'] ?>"><button type="button"><a href="deleteimage.php?id=<?php echo $user['id']; ?>">Delete</a></button></button><input type="file" name="imageUpload"  id="photo" ></td>
					<td><p id="err_photo">*Photo is not valid</p></td> 
				</tr>
				<tr>
					<td>Email: </td>
					<td><input type="email" name="email" value="<?php echo $user['email'] ?>" id="email" ></td>
					<td><p id="err_email">*email is not valid</p></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit"  name="btn" id="btn" value="Submit" class="btn"></td>
					<td style="opacity: 0;">Please Click On Submit</td>
				</tr>
			</tbody>
		</table>
		</form>
	</div>
</body>
<script src="../assets/js/jquery.js"></script>
<script>
		$(window).ready(function(){

		$("p").hide();
	});
</script>
<script src="../assets/js/jquery-custom.js"></script>
</html>
