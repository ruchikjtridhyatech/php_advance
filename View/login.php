<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Login Page</title>
	<link rel="stylesheet" href="assets/css/style.css">

</head>
<body>
	<div class="form">
		<form method="POST" accept-charset="utf-8" >
		<table class="tbl">
			<h2>LOGIN</h2>
			<tbody>
				<tr>
					<td>Email: </td>
					<td><input type="email" name="email" id="email_login"  value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : ''; ?>" required></td>
				</tr>
				<tr>
					<td>Password: </td>
					<td><input type="password" name="password" placeholder="Require Capital, Special Char. and Digit & Min 8 letters" value="<?php echo isset($_COOKIE['password']) ? $_COOKIE['password'] : ''; ?>" id="pass_login" required></td>
				</tr>
				<tr>
					<td></td>
					<td><label class="checkb"><input class="checkbx" type="checkbox" name="checkbx" id="checkbx" >Remember Me </label></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit"  name="btn" id="btn" value="Submit" class="btn">
					<input type="reset"  name="reset" id="reset" value="Reset" class="btn"></td>
				</tr>
				
			</tbody>
		</table>
		</form>
	</div>
</body>
</html>
