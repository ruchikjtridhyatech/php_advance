
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Regestration Page</title>
	<link rel="stylesheet" href="../assets/css/style.css">

</head>
<body>
	<div class="form">
		<form accept-charset="utf-8" method="POST"  enctype="multipart/form-data">
		<table class="tbl">
			<h2>Registration Form</h2>
			<tbody>
				<tr>
					<td>Fisrt Name:</td>
					<td><input type="text" name="fname" id="fname" value="<?php echo isset($_POST['fname']) ? $_POST['fname'] : ''; ?>"></td>
					<td><p id="err_fname">*Please Enter Your First Name!</p></td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td><input type="text" name="lname" id="lname" value="<?php echo isset($_POST['lname']) ? $_POST['lname'] : ''; ?>"></td>
					<td><p id="err_lname">*Please Enter Your Last Name!</p></td>
				</tr>
				<tr>
					<td>Gender :</td>
					<td><label>Male<input type="radio" name="gender" id="gender"  value="male"  required></label>
						<label>Female<input type="radio" name="gender"  id="gender" value="female"></label></td>
					<td><p id="err_gender">*Please select your gender!</p></td>
				</tr>
				<tr>
					<td>Hobby :</td>
					<td>
						<label>Reading<input type="checkbox" name="hobby[]" id="hobby" class="dancing" value="Reading" ></label>
						<label>Surfing<input type="checkbox" name="hobby[]" id="hobby" class="singing" value="Surfing" ></label>
						<label>Cricket<input type="checkbox" name="hobby[] " id="hobby" class="sports" value="Cricket" ></label>
					</td>
					<td><p id="err_hoobie">*Please select any Hoobie!</p></td>
				</tr>
				<tr>
					<td>Phone Number: </td>
					<td><input type="number" name="num" id="num"  value="<?php echo isset($_POST['num']) ? $_POST['num'] : ''; ?>"></td>
					<td><p id="err_num">*Number is not valid</p></td>
				</tr>
				<tr>
					<td>Photo: </td>
					<td><input type="file" name="imageUpload" id="photo" value="<?php echo isset($_POST['image']) ? $_POST['image'] : ''; ?>" required></td>
					<td><p id="err_photo">*Photo is not valid</p></td> 
				</tr>
				<tr>
					<td>Email: </td>
					<td><input type="email" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>"></td>
					<td><p id="err_email">*email is not valid</p></td>
				</tr>
				<tr>
					<td>Password: </td>
					<td><input type="password" name="pass"  placeholder="Require Capital, Special Char. and Digit & Min 8 letters" id="pass" ></td>
					<td><p id="err_pass1">Password is wrong</p></td>
				</tr>
				<tr>
					<td>Confirm Password: </td>
					<td><input type="password" name="pass2" id="pass2" ></td>
					<td><p id="err_pass2">*password does not matched</p></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit"  name="btn" id="btn" value="Submit" class="btn">
				<input type="reset" value="Reset" class="btn"></td>
					<td style="opacity: 0;">Please Click On Submit</td>
				</tr>
				<tr>
					<td colspan="2"><a href="../">Exit</a></td>
				</tr>
			</tbody>
		</table>
		</form>
	</div>
</body>
<script src="../assets/js/jquery.js"></script>
<script>
		$(window).ready(function(){

		$("p").hide();
	});
</script>
<!-- <script src="../assets/js/jquery-custom.js"></script> -->
</html>
