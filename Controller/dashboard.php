<?php 
session_start();
$loginId = $_SESSION['loginId'];
$dashboard = new dashboard($loginId);

class dashboard
{	
	function __construct($userId)
	{
		include '../Model/model.php';
		$db = new Database();
		$conn = $db -> connection();
		$users = $db->  dashboard($conn);
		include '../View/dashboard.php';
	}
}

?>