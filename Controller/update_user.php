<?php 
session_start();
$uid=$_SESSION['uid'];

$update= new Update_User($uid);


class Update_User
{	
	function __construct($uid)
	{
		$image_name=$_SESSION['image'];
		include '../Model/model.php';
		$db = new Database();
		$conn = $db -> connection();
		if(isset($_POST['btn']))
		{
			$firstName = isset($_POST['fname']) ? (string)$_POST['fname'] : "";
			$lastName = isset($_POST['lname']) ? (string)$_POST['lname'] : "";
			$gender = isset($_POST['gender']) ? (string)$_POST['gender'] : "";
			$number = isset($_POST['num']) ? (int)$_POST['num'] : "";
		
			$email = isset($_POST['email']) ? (string)$_POST['email'] : "";
			$hobby_array = isset($_POST['hobby']) ? $_POST['hobby'] : "";
			$hobby=implode(",",$hobby_array);

			if($image_name!='' && $_FILES['imageUpload']['name']=='')
			{
				$imageUpload=$image_name;
			}
			else
			{
				$imageUpload = ($_FILES['imageUpload']['name'])!=""  ? $_FILES['imageUpload']['name'] : "download.png";
			}
			


			// Image Upload
			//$imageUpload = ($_FILES['imageUpload']['name'])!=""  ? $_FILES['imageUpload']['name'] : "download.png"; 

		     $target_dir = "../assets/images/";
		     $target_file = $target_dir .basename($_FILES["imageUpload"]["name"]);
		     $uploadOk = 1;
		    
		     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		     if(isset($_FILES['imageUpload']))
		     {

		        $check = getimagesize($_FILES["imageUpload"]["tmp_name"]);
		        if($check !== false) {
		            //echo "File is an image - " . $check["mime"] . ".";
		            $uploadOk = 1;
		        } else {
		            echo "File is not an image.";
		            $uploadOk = 0;

		     }
		     if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		     && $imageFileType != "gif" ) {
		         echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		         $uploadOk = 0;
		     }
		     // Check if $uploadOk is set to 0 by an error
		     if ($uploadOk == 0) {
		         echo "Sorry, your file was not uploaded.";
		     // if everything is ok, try to upload file
		     } else {
		         if (move_uploaded_file($_FILES["imageUpload"]["tmp_name"], $target_file)) {
		             //echo "The file ". basename( $_FILES["imageUpload"]["name"]). " has been uploaded.";
		         } else {
		             echo "Sorry, there was an error uploading your file.";
		         }

		     		}
			}	


			$data=$db->update_record($conn,$uid,$firstName,$lastName,$gender,$hobby,$number,$imageUpload,$email);
			if($data)
			{
				header("location: dashboard.php");
			}

		}
		
		
	}
}

?>