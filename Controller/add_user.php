<?php 
$update= new Add();


class Add
{	
	function __construct()
	{
		include '../Model/model.php';
		$db = new Database();
		$conn = $db -> connection();
		include '../View/add_user.php';
		
		if(isset($_POST['btn']))
		{	
			$cnt=0;
			$firstName = isset($_POST['fname']) ? (string)$_POST['fname'] : '';
			$lastName = isset($_POST['lname']) ? (string)$_POST['lname'] : '';
			$gender = isset($_POST['gender']) ? (string)$_POST['gender'] : '';
			$hobby_array = isset($_POST['hobby']) ? $_POST['hobby'] : "";
			if($hobby_array!= '')
			{
				$hobby=implode(",",$hobby_array);

			}
			else{
				$hobby='';
			}
			$number = isset($_POST['num']) ? (int)$_POST['num'] : '';
			
			$email = isset($_POST['email']) ? (string)$_POST['email'] : '';
			$password1 = isset($_POST['pass']) ? (string)$_POST['pass'] : '';
			$password2 = isset($_POST['pass2']) ? (string)$_POST['pass2'] : '';

			// Image Upload
			$imageUpload = ($_FILES['imageUpload']['name'])!=""  ? $_FILES['imageUpload']['name'] : "download.png"; 

		     $target_dir = "../assets/images/";
		     $target_file = $target_dir . basename($_FILES["imageUpload"]["name"]);
		     $uploadOk = 1;
		     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		     if(isset($_FILES['imageUpload']))
		     {

		        $check = getimagesize($_FILES["imageUpload"]["tmp_name"]);
		        if($check !== false) {
		            //echo "File is an image - " . $check["mime"] . ".";
		            $uploadOk = 1;
		        } else {
		            echo "File is not an image.";
		            $uploadOk = 0;

		     }
		     if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		     && $imageFileType != "gif" ) {
		         echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		         $uploadOk = 0;
		     }
		     // Check if $uploadOk is set to 0 by an error
		     if ($uploadOk == 0) {
		         echo "Sorry, your file was not uploaded.";
		     // if everything is ok, try to upload file
		     } else {
		         if (move_uploaded_file($_FILES["imageUpload"]["tmp_name"], $target_file)) {
		             //echo "The file ". basename( $_FILES["imageUpload"]["name"]). " has been uploaded.";
		         } else {
		             echo "Sorry, there was an error uploading your file.";
		         }

		     		}
			}	

			//validations of Not Null Fiels		
			if($firstName!='' &&  $lastName!='' && $gender!= '' && $number!=''  && $email!='' && $password1!='' && $password2!='')
			{
				
				//Phone should be 10 digit	
				if(strlen($number)==10)
				{	
					$email_rev=strrev ( $email) ;
					$email_com=explode(".", $email_rev);

					//valid email
					if(strlen($email_com[0]) < 4)
					{

						$email_valid=$db->valid_email($email,$conn);
						//email not exixts
						if($email_valid)
						{
							//password matching
							if(strlen($password1) >= 8)
							{
								
								if(preg_match("/[A-Z]/", $password1) && preg_match("/[0-9]/", $password1) && preg_match("/[&^%$#@!*]/", $password1))
								{

									if($password1 == $password2)
									{
										$data=$db->add_record($conn,$firstName,$lastName,$gender,$hobby,$number,$imageUpload,$email,$password1);
										if($data)
										{
											header("location: dashboard.php");
										}
									}
									else
									{
										$cnt=7;
										include "error.php";
									}
									
								}
								else
								{
									$cnt=6;
									include "error.php";
									
								}
								
							}
							else
							{ 
								$cnt=5;
								include "error.php";
							}
						}
						else
						{
							$cnt=4;
							include "error.php";
						}
						
					}
					else
					{
						$cnt=3;
						include "error.php";
					}
					
					
					
				}
				else
				{
					$cnt=2;
					include "error.php";
				}
					
			}
			else
			{
				$cnt=1;
				include "error.php";
				
			}

			
		}
		
	}
}

?>