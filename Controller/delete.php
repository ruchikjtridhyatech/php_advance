<?php 
session_start();
$uid = isset($_GET['id']) ? (int)$_GET['id'] :"";
$delete= new Delete($uid);

class Delete
{	
	function __construct($uid)
	{
		include '../Model/model.php';
		$db = new Database();
		$conn = $db -> connection();
		$data = $db->  delete($conn,$uid);
		
		if(isset($data) && $data == 1)
		{	
			$users = $db->  dashboard($conn);
			include '../View/dashboard.php';
		}
		
	}
}

?>