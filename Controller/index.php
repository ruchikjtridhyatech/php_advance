<?php 
session_start();	
	class Controller {
	
		// db connection
		public function __construct()
		{
			include "Model/model.php";
			$db = new Database();
			$conn=$db -> connection();
			include 'View/login.php';

			//User Authentication
			if(isset($_POST['btn']))
			{
				$userName = isset($_POST['email']) ? (string)$_POST['email'] : "";
				$password = isset($_POST['password']) ? (string)$_POST['password'] : "";

				//login Method of Model
				$login=$db -> user_login($userName,$password,$conn);
				if($login != 0)
				{

					if(isset($_POST['checkbx']))
					{

						$cookie_name="email";
						$cookie_value=$userName;
						setcookie($cookie_name,$cookie_value, time()+(60*60*24*30));

						$cookie_name="password";
						$cookie_value=$password;
						setcookie($cookie_name,$cookie_value, time()+(60*60*24*30));
					}
						$_SESSION['loginId'] = $login;

						header("location: Controller/dashboard.php");
						exit;
					
				}
				else
				{
					?>
					<script>
						alert("User Name Or Password Is Wrong");
					</script>
					<?php
					//print_r($_POST);
				}
			}
			$login = isset($login) ? $login : "";
		}
		
	}
?>