<?php 
session_start();
$loginId = $_SESSION['loginId'];
$logout = new logout($loginId);

class logout
{	
	function __construct($logId)
	{
		session_destroy();
		
		header('location:../');
	}
}

?>