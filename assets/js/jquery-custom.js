
	$(document).ready(function(){

		
		var cnt_fname=1;
		var cnt_lname=1;
		var cnt_num=1;
		var cnt_add=1;
		var cnt_email=1;
		var cnt_pass1=1;
		var cnt_pass2=1;
	

		// On Name Textbox
		$("#fname").blur(function(){
			var name=$(this).val();
			if(name==''){
				$("#err_fname").show();
				 cnt_fname=1;
				  // alert(cnt_add);
			}
		});
		$("#fname").focus(function(){
			 $("#err_fname").hide();
			  cnt_fname=0;
		});

		// On Name Textbox
		$("#lname").blur(function(){
			var name=$(this).val();
			if(name==''){
				$("#err_lname").show();
				 cnt_lname=1;
				  // alert(cnt_add);
			}
		});
		$("#lname").focus(function(){
			 $("#err_lname").hide();
			  cnt_lname=0;
		});

		// On Phone number
		$("#num").blur(function(){
			var num=$(this).val();
			if(num==''){
				$("#err_num").show();
				cnt_num=1;
			}
			if(num.length != 10){
				$("#err_num").show();
				cnt_num=1;
			}
		});
		$("#num").focus(function(){
			 $("#err_num").hide();
			 cnt_num=0;
		});


		// On Address
		$("#address").blur(function(){
			var address=$(this).val();
			if(address==''){
				$("#err_address").show();
				cnt_add=1;
				// alert(cnt_add);
			}
		});
		$("#address").focus(function(){
			 $("#err_address").hide();
			 cnt_add=0;
			 // alert(cnt_add);
		});

		
		// On Email
		$("#email").blur(function(){
			var email=$(this).val();
			var email_len=email.split('.').reverse();
			if(email==''){
				$("#err_email").show();
				 cnt_email=1;
			}
			if(email_len[0].length > 3)  {
				$("#err_email").show();
				 cnt_email=1;
			}
			else
			{
				$("#err_email").hide();
				 cnt_email=0;
			}
		});
		$("#email").focus(function(){
			 $("#err_email").hide();
			  cnt_email=0;
		});

		// On Password Validation
		$("#pass").blur(function(){
			var pass=$(this).val();
			if(pass.length >=8)
			{
				if(pass.match(/[A-Z]/) && pass.match(/[0-9]/) && pass.match(/[&^%$#@!*]/))
				{
					var pass1=$(this).val();
					var pass2=$("#pass2").val();
						if(pass1!=pass2) // password matching
						{
							$("#err_pass2").show();
									 cnt_pass2=1;
						}
						else
						{

							$("#err_pass1").hide();
							cnt_pass1=0;
						}
				}
				else
				{
					$("#err_pass1").show();
							cnt_pass1=1;
				}
			}
			else
			{
				$("#err_pass1").show();
						 cnt_pass1=1;
			}
		});
		$("#pass").focus(function(){
			 $("#err_pass1").hide();
			 		 cnt_pass1=0;
		});

		// On Password Matching
		$("#pass2").blur(function(){
			var pass1=$(this).val();
			var pass2=$("#pass").val();
				if(pass1!=pass2) // password matching
				{
					$("#err_pass2").show();
							 cnt_pass2=1;
				}
		});
		$("#pass2").focus(function(){
			 $("#err_pass2").hide();
			  cnt_pass2=0;
		});
		
	});
